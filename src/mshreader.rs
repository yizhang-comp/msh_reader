use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn read_mesh_from_msh(filename: &String) ->
//  (nnod , node IDs  , node coord   , nelem, elem IDs  , ele types , node IDs in ele)
    (usize, Vec<usize>, Vec<Vec<f64>>, usize, Vec<usize>, Vec<usize>, Vec<Vec<usize>>){

        let file = File::open(filename).expect("Unable to open file");
        let reader = BufReader::new(&file);

        let mut sec_node = false; let mut read_node_number = true;
        let mut sec_elem = false; let mut read_elem_number = true;

        // node data
        let (mut nnode, mut nid, mut coord) = (0, vec![], vec![]);
        // elem data
        let (mut nelem, mut eid, mut etype, mut nids) = (0, vec![], vec![], vec![]);

        let mut ntags = 0;

        for line in reader.lines(){
            match line {
                Ok(s) => { if s == "$Nodes"    {sec_node = true; continue;} else if s == "$EndNodes"    {sec_node = false; continue;}
                           if s == "$Elements" {sec_elem = true; continue;} else if s == "$EndElements" {sec_elem = false; continue;}
                           if sec_node {
                               if read_node_number {nnode = s.parse::<usize>().unwrap(); read_node_number = false; continue;} 
                               else {
                                   let iter = s.split_whitespace().enumerate();
                                   let mut c = vec![];
                                   for (fieldnumber, field) in iter {
                                       if fieldnumber == 0 {nid.push(field.parse::<usize>().unwrap());}
                                       else {c.push(field.parse::<f64>().unwrap());}
                                   }
                                   coord.push(c);
                               }
                           } else if sec_elem {
                               if read_elem_number {nelem = s.parse::<usize>().unwrap(); read_elem_number = false; continue;}
                               else {
                                   let mut d = vec![];
                                   let iter = s.split_whitespace().enumerate();
                                   for (fieldnumber, field) in iter {
                                       if fieldnumber == 0 {eid.push(field.parse::<usize>().unwrap());}
                                       else if fieldnumber == 1 {etype.push(field.parse::<usize>().unwrap());}
                                       else if fieldnumber == 2 {ntags = field.parse::<usize>().unwrap();}
                                       else if (fieldnumber > 2) & (fieldnumber < 3+ntags) {}// todo ntags
                                       else {d.push(field.parse::<usize>().unwrap());}
                                   }
                                   nids.push(d);
                               }
                           }
                }
                _ => println!("Error"),
            };
        }
        (nnode, nid, coord, nelem, eid, etype, nids)
    }


#[test]
fn read_circle() {
    let filename = "/home/yzhang/Documents/work/newtank/lib/mesh2/mshparser/src/circle_2d.msh".to_string();
    let (nnode, nid, coord, nelem, eid, etype, nids) = read_mesh_from_msh(&filename);
    println!("Number of nodes: {}", nnode);
    for i in 0..nnode{
        println!("Node {} at {:?}", nid[i], coord[i]);
    }
    println!("Number of elements: {}", nelem);
    for i in 0..nelem{
        println!("Element {} of type {} with nodes {:?}", eid[i], etype[i], nids[i]);
    }
}
